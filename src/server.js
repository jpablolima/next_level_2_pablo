// Server

const express = require('express');
const server = express();
const { pageLanding, pageStudy, pageGiveClasses, saveClasses } = require('./pages');
const nunjucks = require('nunjucks');

// configuração do Nunjucks
nunjucks.configure('src/views', {
    express: server,
    noCache: true,
});

server
// configuração de arquivos estáticos (css, scripts, imagens)
    .use(express.urlencoded({ extended: true }))
    .use(express.static('public'))
    // rotas da aplicação
    .get("/", pageLanding)
    .get("/study", pageStudy)
    .get("/give-classes", pageGiveClasses)
    .post('/save-classes', saveClasses)
    .listen(5500);